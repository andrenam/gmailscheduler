Schedule E-Mails in Gmail without relying on external services. 

Requirements: Python 2.6 or 2.7, pytz, python-dateutil

Usage: 
------
Save an E-Mail in the drafts folder with the following keyword in the subject:

	[Scheduled@time]
	
where "time" is a time or datetime in a format that can be recognized by 
python-dateutil. For example 
	
	[Scheduled@10:36]
	[Scheduled@10h am]
	[Scheduled@2013-09-25T10:49:41.5-03:00]

and a lot more...
Alternatively, you can use [Scheduled@+04:15] to schedule the mail in 4 hours and 15 minutes.

Run the script periodically to check for scheduled mails, for example as cronjob every 15 minutes.


